from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    import_href         = models.URLField(max_length=3000)
    vin                 = models.CharField(max_length=20)

class Technician(models.Model):
    name                = models.CharField(max_length=200)
    employee_number     = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class Appointments(models.Model):
    customer_name       = models.CharField(max_length=200)
    vin                 = models.CharField(max_length=20)
    date_created        = models.DateTimeField(auto_now_add=True)
    appointment_reason  = models.TextField()
    vip                 = models.BooleanField()
    technician          = models.ForeignKey(Technician, related_name="appointements", on_delete=models.CASCADE)

    def __str__(self):
        return self.customer_name


from django.urls import path

from .api_views import api_list_technicians, api_list_appointments, api_show_appointment

urlpatterns = [
    path('service/technicians/', api_list_technicians, name="api_list_technicians"),
    path('service/appointments/', api_list_appointments, name="api_list_appointments"),
    path('service/appointments/<int:pk>/', api_show_appointment, name="api_show_appointment")
]
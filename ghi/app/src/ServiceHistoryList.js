import React from 'react'

class ServiceHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: '',
            appointments: []

        };
    };


    handleInput = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name] : value});
    };
    handleSearch = async (event) => {
        event.preventDefault();
       let filterAppointments = this.state.appointments.filter(appointment => appointment.vin === this.state.vin)
       this.setState({"appointments": filterAppointments})


    };

    async componentDidMount() {
        const url = 'http://localhost:8080/api/service/appointments/';
    
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ appointments: data.appointments });
        }
      }

    render () {
        return (
            <div className='form'>
                <h1>Service History</h1>
                <form onSubmit={this.handleSearch}>
                    <label htmlFor="search">Search VIN: </label>
                    <input onChange={this.handleInput}id="search" type="" placeholder="Enter VIN#" name="vin" />
                    <button>Search</button>
                </form>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">VIN</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Technician</th>
                            <th scope="col">Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => {
                            return (
                                <tr>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer_name}</td>
                                    <td>{new Date(appointment.date_created).toLocaleDateString()}</td>
                                    <td>{new Date(appointment.date_created).toLocaleTimeString()}</td>
                                    <td>{appointment.technician.name}</td>
                                    <td>{appointment.appointment_reason}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>

            </div>
        );
    }; 
}
export default ServiceHistoryList;
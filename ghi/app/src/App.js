import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesRecord from "./Sales/RecordList";
import NewCustomerForm from './Sales/NewCustomerForm';
import NewSalesPersonForm from './Sales/NewSalesmanForm';
import NewSaleForm from './Sales/NewSaleForm';
import SalesRecordbyPerson from './Sales/SalesRecordbyPerson';
import CustomerList from './Sales/CustomerList';
import AddManufacturer from './Inventory/ManufacturerForm';
import AddModel from './Inventory/ModelForm';
import AddAutomobile from './Inventory/AutomobileForm';
import AutomobileList from './Inventory/AutomobileList';
import ManufacturerList from './Inventory/ManufacturerList';
import ModelList from './Inventory/ModelList';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import ServiceAppointmentList from './ServiceAppointmentList';
import ServiceHistoryList from './ServiceHistoryList';
import TechnicianForm from './TechnicianForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales" element={<SalesRecord />} />
          <Route path="/customer" element={<NewCustomerForm />} />
          <Route path="/employees" element={<NewSalesPersonForm />} />
          <Route path="/newsale" element={<NewSaleForm />} />
          <Route path="/employeelog" element={<SalesRecordbyPerson />} />
          <Route path="/customerlist" element={<CustomerList />} />
          <Route path="/addmanufacturer" element={<AddManufacturer />} />
          <Route path="/addmodel" element={<AddModel />} />
          <Route path="/addvehicle" element={<AddAutomobile />} />
          <Route path="/listvehicle" element={<AutomobileList />} />
          <Route path="/listmanufacturers" element={<ManufacturerList />} />
          <Route path="/listmodels" element={<ModelList />} />
          <Route path="/service/" element={<ServiceAppointmentList />} />
          <Route path="/service/history" element= {<ServiceHistoryList/>} />
          <Route path="/service/new" element={<ServiceAppointmentForm />} />
          <Route path="/techician/new" element={<TechnicianForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

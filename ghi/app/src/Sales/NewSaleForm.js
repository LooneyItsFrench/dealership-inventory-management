import React from 'react';

class NewSaleForm extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
        salespersons: [],
        customers: [],
        automobiles: [],
        sale_price: '',
    }
}
async componentDidMount() {
  const automobileResponse = await fetch(`http://localhost:8090/api/sales/unsold-vehicles/`);
  const salesPersonResponse = await fetch(`http://localhost:8090/api/sales/employees/`);
  const customerResponse = await fetch(`http://localhost:8090/api/sales/customers/`);
  if (automobileResponse.ok && salesPersonResponse && customerResponse.ok) {
    const automobileData = await automobileResponse.json();
    const salesPersonData = await salesPersonResponse.json();
    const customerData = await customerResponse.json();
    this.setState({ automobiles: automobileData.automobiles },
    this.setState({ salespersons: salesPersonData.salesperson}),
    this.setState({ customers: customerData.customers })
    );
  }
}

handleChange = event => {
    const {name, value} = event.target;
    this.setState({
        [name]: value
    });
}
handleSubmit = async event => {
  event.preventDefault();
  const data = {...this.state};
  delete data.automobiles;
  delete data.customers;
  delete data.salespersons;
  console.log(data)
  const response = await fetch(
    `http://localhost:8090/api/sales/`, 
      { 
      method: "post",
      body: JSON.stringify(data),
      headers: {'Content-Type': 'application/json'}
      }
  );
  if (response.ok) {
    const cleared = {
      automobiles: [],
      salespersons: [],
      customers: [],
      sale_price: '',
    };
    this.setState(cleared);
  }
}

render() {
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={this.handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select onChange={this.handleChange} required name="vin" id="automobile" className="form-select">
              <option value="">Choose an vehicle</option>
                {this.state.automobiles.map(automobile => {
                  return (
                  <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={this.handleChange} required name="sales_person" id="sales_person" className="form-select">
              <option value="">Who sold the vehicle?</option>
                {this.state.salespersons.map(sales_person=> {
                  return (
                  <option key={sales_person.name} value={sales_person.name}>{sales_person.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={this.handleChange} required name="customer" id="customer" className="form-select">
              <option value="">Who bought the vehicle?</option>
              {this.state.customers.map(customer=> {
                return (
                <option key={customer.name} value={customer.name}>{customer.name}</option>
                )
              })}
              </select>
            </div>
          <div className="form-floating mb-3">
            <input onChange={this.handleChange} value={this.state.sale_price} placeholder="sale_price" required type="number" name="sale_price" id="sale_price" className="form-control" />
            <label htmlFor="price">Sale price</label>
          </div> 
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
)
}
}
export default NewSaleForm

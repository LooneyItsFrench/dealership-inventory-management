import React from 'react';

class NewCustomerForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          customer_address: {
            address: '',
            city: '',
            zipcode: '',
            state: '',
            country: '',
          },
          phonenumber: '',
        };
      }
      handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
      }
      handleChangeAddress = event => {
        const {name, value} = event.target;
        console.log(name)
        console.log(value)
        const test = {...this.state.customer_address}
        this.setState( state => {
          state.customer_address[name] = value
          return state
        });
      }
      handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        console.log(data)
        const customerUrl = `http://localhost:8090/api/sales/customers/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
          const cleared = {
            name: '',
            customer_address: {
              address: '',
              city: '',
              zipcode: '',
              state: '',
              country: '',
            },
            phonenumber: ''
          };
          this.setState(cleared);
        }
      }

    render(){
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Please add a Customer</h1>
                <form onSubmit={this.handleSubmit} id="create-customer-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.name} placeholder="Customer Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.customer_address.address} placeholder="Customer Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Street Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.customer_address.city} placeholder="Customer City" required type="text" name="city" id="city" className="form-control" />
                    <label htmlFor="City">City</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.customer_address.state} placeholder="Customer Address" required type="text" name="state" id="state" className="form-control" />
                    <label htmlFor="State">State</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.customer_address.zipcode} placeholder="Customer Zipcode" required type="text" name="zipcode" id="zipcode" className="form-control" />
                    <label htmlFor="Zipcode">Zipcode</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.customer_address.country} placeholder="Customer Country" required type="text" name="country" id="country" className="form-control" />
                    <label htmlFor="Country">Country</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.phonenumber} placeholder="Customer Phone Number" required type="text" name="phonenumber" id="phonenumber" className="form-control" />
                    <label htmlFor="phone_number">Customer Phone Number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

export default NewCustomerForm

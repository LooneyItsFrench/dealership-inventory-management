import React from 'react';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name : '',
            employee_number : '',
        };
    };

    handleInput = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name] : value});
    };

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state };

        const technicianDetailURL = 'http://localhost:8080/api/service/technicians/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(technicianDetailURL, fetchConfig);
        if (response.ok) {
            event.target.reset();
        };

    };

    render () {
        return (
            <div className='form'>
                <h1>Service Technician</h1>
                <form onSubmit={this.handleSubmit}>
                        <table>
                            Name: 
                            <input onChange={this.handleInput} type="text" placeholder='Technician' name='name' />
                            Employee ID:
                            <input onChange={this.handleInput}type="text" placeholder='Employee Number' name='employee_number' />
                            <input type='submit' value='Submit' />
                         </table>
                    
                </form>

            </div>
        )
    };
};

export default Form;
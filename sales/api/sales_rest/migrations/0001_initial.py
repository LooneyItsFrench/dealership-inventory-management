# Generated by Django 4.0.3 on 2022-10-25 13:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=50)),
                ('city', models.CharField(default='Austin', max_length=60)),
                ('state', models.CharField(default='Texas', max_length=30)),
                ('zipcode', models.CharField(default='78759', max_length=5)),
                ('country', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('phonenumber', models.CharField(max_length=50)),
                ('customer_address', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='customer', to='sales_rest.address')),
            ],
        ),
        migrations.CreateModel(
            name='SalesPerson',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('employeeid', models.PositiveIntegerField(verbose_name='')),
            ],
        ),
        migrations.CreateModel(
            name='VehicleVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.CharField(max_length=17, unique=True)),
                ('import_href', models.CharField(max_length=200, null=True, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='SaleRecord',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sale_price', models.PositiveIntegerField(verbose_name='')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='salerecord', to='sales_rest.customer')),
                ('sales_person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='salerecord', to='sales_rest.salesperson')),
                ('vin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='salerecord', to='sales_rest.vehiclevo')),
            ],
        ),
    ]
